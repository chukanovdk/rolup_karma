import nodeResolve from 'rollup-plugin-node-resolve';
import commonJs from 'rollup-plugin-commonjs';
import {sizeSnapshot} from "rollup-plugin-size-snapshot";
import {terser} from 'rollup-plugin-terser';
import babel from 'rollup-plugin-babel';
import istanbul from 'rollup-plugin-istanbul';

const build = process.env.NODE_ENV ==="production";

function pluginsInit(build) {
 if(build) {
   return [
    istanbul({
      exclude: ['src/**/*.spec.js']
    }),
    babel({
      exclude: 'node_modules/**'
    }),
    nodeResolve(), // подключение модулей node
    commonJs(), // подключение модулей commonjs
    sizeSnapshot(), // напишет в консоль размер бандла
    terser(), // минификатор совместимый с ES2015+, форк и наследник UglifyES
  ]  
 } else {
   return   [
    babel({
      exclude: 'node_modules/**'
    }),
    istanbul({
      exclude: ['src/**/*.spec.js']
    }),
    nodeResolve(), // подключение модулей node
    commonJs(), // подключение модулей commonjs
    sizeSnapshot(), // напишет в консоль размер бандла
  ]
 }
}

export default [{
  input: 'src/index.js',
  output: [{ file: 'dist/index.min.js', format: 'iife' }],
  plugins: pluginsInit(build),
}];