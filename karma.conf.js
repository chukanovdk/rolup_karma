const Chrome =  require('karma-chrome-launcher') 
const istanbul = require('rollup-plugin-istanbul');
// karma.conf.js
const phantomJs = process.env.NODE_ENV ==="phantomJs";

function initFrameworks() {
	if(phantomJs) {
		return [ 'chai','jasmine']
	} else {
		return  [ 'chai','jasmine','sinon']
	}
}

module.exports = function(config) {
	config.set({
    coverageReporter: {
      dir:'tmp/coverage/',
      reporters: [
        { type:'html', subdir: 'report-html' },
        { type:'lcov', subdir: 'report-lcov' }
      ],
      instrumenterOptions: {
        istanbul: { noCompact:true }
      }
    },
		files: [
			/**
			 * Make sure to disable Karma’s file watcher
			 * because the preprocessor will use its own.
			 */
			{ pattern: 'src/**/*.spec.js', watched: false },
		],

		preprocessors: {
			'src/**/*.spec.js': ['rollup'],
    },
    browsers: ['Chrome','Firefox','Safari','IE'],
    // репортеры необходимы для  наглядного отображения результатов
    frameworks: initFrameworks(),
    reporters: ['mocha', 'coverage'],
		rollupPreprocessor: {
			/**
			 * This is just a normal Rollup config object,
			 * except that `input` is handled for you.
			 */
			plugins: [require('@rollup/plugin-buble')(), istanbul({
        exclude: ['src/**/*.spec.js']
      })],
			output: {
				format: 'iife', // Helps prevent naming collisions.
				name: 'Test', // Required for 'iife' format.
				sourcemap: 'inline', // Sensible for testing.
			},
		},
	})
}