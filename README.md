# rolup_karma

---

npm run         | Содержание файла
----------------|----------------------
build           | Билд production
start           | Билд development
test            | Тесты без sinon в PhantomJS
test_all        | Тесты с sinon по всем браузерам
test_chrome     | Тесты с sinon  для chrome
test_ie         | Тесты с sinon  для ie
test_firefox    | Тесты с sinon  для firefox 
test_safari     | Тесты с sinon  для safari







`https://www.chaijs.com/api/` - Документация методов
---
`https://karma-runner.github.io/4.0/config/configuration-file.html` - Документайция Karma